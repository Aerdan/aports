# Contributor: Sodface <sod@sodface.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=satty
pkgver=0.12.1
pkgrel=0
pkgdesc="Screenshot annotation tool"
url="https://github.com/gabm/Satty"
arch="all"
license="MPL-2.0"
makedepends="
	cargo
	cargo-auditable
	gtk4.0-dev
	libadwaita-dev
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="!check" # no test suite
source="$pkgname-$pkgver.tar.gz::https://github.com/gabm/Satty/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/Satty-$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

package() {
	install -Dm755 target/release/satty -t "$pkgdir"/usr/bin

	install -Dm644 satty.desktop \
		-t "$pkgdir"/usr/share/applications/
	install -Dm644 assets/satty.svg \
		-t "$pkgdir"/usr/share/icons/hicolor/scalable/apps

	install -Dm644 completions/satty.bash \
		"$pkgdir"/usr/share/bash-completion/completions/satty
	install -Dm644 completions/satty.fish \
		-t "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/_satty \
		-t "$pkgdir"/usr/share/zsh/site-functions
}

sha512sums="
516cb85d2dff0bd65339e57f3e1079d386071ef32c8c9c3328148418ce612bbc350b6cb3a262674445bae723adce4fedfcf75de1fef2b92ae4426b374c0e6855  satty-0.12.1.tar.gz
"
